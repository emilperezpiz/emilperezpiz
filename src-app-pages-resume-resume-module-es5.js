function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["src-app-pages-resume-resume-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/resume/resume.component.html":
  /*!******************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/resume/resume.component.html ***!
    \******************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesResumeResumeComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div id=\"resume\">\r\n    <div class=\"row\">\r\n        <div class=\"col-lg-6 col-md-6\">\r\n            <div class=\"text animate__animated animate__fadeIn\">\r\n                <h2 class=\"mb-4 mt-5\">{{ 'title.myResume' | translate }}</h2>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"row animate__animated animate__fadeIn\">\r\n        <div class=\"col-lg-6 col-md-6 col-sm-12\">\r\n            <div class=\"container-caixa\">\r\n                <h3 class=\"scheduled\">11/2019 - Atualidade</h3>\r\n                <h4 class=\"heading\">Analista Desenvolvedor Full Stack</h4>\r\n                <h6 class=\"company mb-3\"><a href=\"http://www.unisolution.com.br/\" target=\"_blank\"\r\n                        rel=\"nofollow\">UniSolution - Consultoria e Desenvolvimento de Sistemas</a></h6>\r\n                <span>Participação em um projeto de migração do sistema Desktop para Web, com foco no gerenciamento de\r\n                    Terminais e Armazéns Gerais, utilizando as linguagens Angular 8, HTML5, Bootstrap, C# com ASP.Net\r\n                    Framework e Web Api para\r\n                    construção de serviços REST; utilização do padrão arquitetural DDD, Dapper e Entity Framework como\r\n                    ORM para manipulação de dados no SQL Server.</span>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-lg-6 col-md-6 col-sm-12\">\r\n            <div class=\"container-caixa\">\r\n                <h3 class=\"scheduled\">01/2019 - 11/2019</h3>\r\n                <h4 class=\"heading\">Desenvolvedor Full Stack</h4>\r\n                <h6 class=\"company mb-3\"><a href=\"http://www.pravy.com.br/\" target=\"_blank\" rel=\"nofollow\">Pravy -\r\n                        Estudio Digital de Innovación</a>\r\n                </h6>\r\n                <span>Realização de sites sob demanda para os principais shopping do São Paulo, atuando com as\r\n                    tecnologias PHP (Laravel) no Backend, Angular 6+ ou Aurelia no Frontend, e Ionic no desenvolvimento\r\n                    do aplicativos. Os sites e os aplicativos poderiam favorecer uma campanha específica dos shoppings\r\n                    ou representar a imagem principal dos mesmos.</span>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-lg-6 col-md-6 col-sm-12\">\r\n            <div class=\"container-caixa\">\r\n                <h3 class=\"scheduled\">02/2016 – 11/2018</h3>\r\n                <h4 class=\"heading\">Desenvolvedor Web, Analista, Desenhador de Base de Datos e Jefe de\r\n                    Projeto</h4>\r\n                <h6 class=\"company mb-3\">Xinerbit Software</h6>\r\n                <span>Realização de projetos para diversos clientes de curtoe longo prazo atuando com as tecnologias PHP\r\n                    (Symfony, Laravel), Angular, Ionic, Node e outras.</span>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-lg-6 col-md-6 col-sm-12\">\r\n            <div class=\"container-caixa\">\r\n                <h3 class=\"scheduled\">10/2013 - 02/2016</h3>\r\n                <h4 class=\"heading\">Desenvolvedor Backend e Frontend, analista e designer de banco de dados</h4>\r\n                <h6 class=\"company mb-3\">Freelancer</h6>\r\n                <span>Realização de projetos sob demanda, alguns desde o início, outros apenas para manutenção,\r\n                    principalmente utilizando tecnologias php (laravel, symfony ou Yii), angular 2+ ou angular js,\r\n                    mySQL, API (JSON ou XML).</span>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"row\">\r\n        <div class=\"container-download mt-4\">\r\n            <a href=\"assets/CV_EMILPEREZPIZ.pdf\" download>\r\n                <div class=\"download-button\">\r\n                    <span>{{ 'button.downloadCV' | translate }}</span>\r\n                </div>\r\n            </a>\r\n        </div>\r\n    </div>\r\n</div>";
    /***/
  },

  /***/
  "./src/app/pages/resume/resume.component.scss":
  /*!****************************************************!*\
    !*** ./src/app/pages/resume/resume.component.scss ***!
    \****************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesResumeResumeComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "@import url(\"https://fonts.googleapis.com/css?family=Signika\");\n@import url(\"https://fonts.googleapis.com/css?family=Fira+Sans\");\n@import url(\"https://fonts.googleapis.com/css?family=Zilla+Slab\");\n@import url(\"https://fonts.googleapis.com/css2?family=Fjalla+One&display=swap\");\ndiv#resume div.row {\n  padding-left: 4%;\n  padding-right: 4%;\n}\ndiv#resume div.text h2 {\n  color: #ffffff;\n  font-size: 50px;\n  font-weight: 800;\n  text-shadow: 5px 25px 2px rgba(255, 255, 255, 0.1);\n}\ndiv#resume div.container-caixa {\n  color: rgba(255, 255, 255, 0.7);\n  margin-bottom: 30px;\n  background-color: rgba(255, 255, 255, 0.1);\n  padding: 30px;\n  border-radius: 5px;\n  height: 280px;\n  overflow: auto;\n}\ndiv#resume div.container-caixa::-webkit-scrollbar {\n  width: 8px;\n}\ndiv#resume div.container-caixa::-webkit-scrollbar-track {\n  box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);\n}\ndiv#resume div.container-caixa::-webkit-scrollbar-thumb {\n  background-color: darkgrey;\n  outline: 1px solid slategrey;\n  border-radius: 4px;\n}\ndiv#resume div.container-caixa h3.scheduled {\n  color: #fbb03b;\n  font-weight: 900;\n}\ndiv#resume div.container-caixa h4.heading {\n  color: #ffffff;\n}\ndiv#resume div.container-caixa h6.company {\n  color: rgba(255, 255, 255, 0.7);\n  transition: all 10s ease 0s;\n}\ndiv#resume div.container-caixa h6.company a {\n  color: rgba(255, 255, 255, 0.7);\n  border-bottom: dotted 1px;\n  text-decoration: none;\n}\ndiv#resume div.container-caixa h6.company a:hover {\n  color: rgba(255, 255, 255, 0.7);\n  border-bottom: solid 1px;\n}\ndiv#resume div.container-caixa span {\n  color: rgba(255, 255, 255, 0.7);\n}\ndiv#resume div.container-download {\n  padding: 1% 0;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  width: 100%;\n}\ndiv#resume div.container-download a {\n  width: 150px;\n  text-decoration: none !important;\n}\ndiv#resume div.container-download div.download-button {\n  height: 40px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  background-color: #fbb03b;\n  border: 1px solid #ffbd39;\n  color: #000000;\n  border-radius: 40px;\n  font-size: 12px;\n  text-transform: uppercase;\n  letter-spacing: 2px;\n  font-weight: 600;\n  box-shadow: 0px 24px 36px -11px rgba(0, 0, 0, 0.09);\n}\ndiv#resume div.container-download div.download-button:hover {\n  color: whitesmoke;\n  background-color: #ffbd39;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcmVzdW1lL0M6XFx4YW1wcFxcaHRkb2NzXFxlbWlscGVyZXpwaXovc3JjXFxhcHBcXHNoYXJlZFxcc3R5bGVzXFxkZWNsYXJhdGlvbnMuc2NzcyIsInNyYy9hcHAvcGFnZXMvcmVzdW1lL0M6XFx4YW1wcFxcaHRkb2NzXFxlbWlscGVyZXpwaXovc3JjXFxhcHBcXHBhZ2VzXFxyZXN1bWVcXHJlc3VtZS5jb21wb25lbnQuc2NzcyIsInNyYy9hcHAvcGFnZXMvcmVzdW1lL3Jlc3VtZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFtQ1EsOERBQUE7QUFDQSxnRUFBQTtBQUNBLGlFQUFBO0FBQ0EsK0VBQUE7QUNwQ1I7RUFDSSxnQkFBQTtFQUNBLGlCQUFBO0FDR0o7QURBQTtFQUNJLGNBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrREFBQTtBQ0dKO0FEQUE7RUFDSSwrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsMENBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0VBQ0EsY0FBQTtBQ0dKO0FEQUE7RUFDSSxVQUFBO0FDR0o7QURBRTtFQUNFLDRDQUFBO0FDR0o7QURBRTtFQUNFLDBCQUFBO0VBQ0EsNEJBQUE7RUFDQSxrQkFBQTtBQ0dKO0FEQUE7RUFDSSxjQUFBO0VBQ0EsZ0JBQUE7QUNHSjtBREFBO0VBQ0ksY0FBQTtBQ0dKO0FEQUE7RUFDSSwrQkFBQTtFQUNBLDJCQUFBO0FDR0o7QURBQTtFQUNJLCtCQUFBO0VBQ0EseUJBQUE7RUFDQSxxQkFBQTtBQ0dKO0FEQUE7RUFDSSwrQkFBQTtFQUNBLHdCQUFBO0FDR0o7QURDQTtFQUNJLCtCQUFBO0FDRUo7QURDQTtFQUNJLGFBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLFdBQUE7QUNFSjtBRENBO0VBQ0ksWUFBQTtFQUNBLGdDQUFBO0FDRUo7QURDQTtFQUNJLFlBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0EseUJBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EseUJBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsbURBQUE7QUNFSjtBRENBO0VBQ0ksaUJBQUE7RUFDQSx5QkFBQTtBQ0VKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvcmVzdW1lL3Jlc3VtZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8vIENPTE9SU1xyXG4kY29sb3JzOiAoXHJcbiAgX3llbGxvd18xOiAjZmJiMDNiLFxyXG4gIF95ZWxsb3dfMjogI2ZmYmQzOSxcclxuICBfZ3JheV8xOiAjYjBiNGM3LFxyXG4gIF9ncmF5XzI6ICNjOGM4YzgsXHJcbiAgX2dyYXlfMzogIzZmNzc4MyxcclxuICBfZ3JheV80OiAjNGE0YjVhLFxyXG4gIF9ncmF5XzU6ICM3OTdkOWYsXHJcbiAgX2dyYXlfNjogIzcwNzA3MCxcclxuICBfZ3JheV83OiAjY2NjLFxyXG4gIF9ncmF5Xzg6ICNmN2Y5ZmMsXHJcbiAgX2dyYXlfOTogI2VkZjFmNyxcclxuICBfd2hpdGVfMTogI2ZmZmZmZixcclxuICBfd2hpdGVfMjogI2RkZjhmYixcclxuICBfd2hpdGVfMzogI2ZmY2M5OCxcclxuICBfd2hpdGVfNDogI2Q3ZTRjNSxcclxuICBfd2hpdGVfNTogI2Y3ZjlmYyxcclxuICBfd2hpdGVfNjogI2VkZjFmNyxcclxuICBfYmx1ZV8xOiAjMTRiMmQzLFxyXG4gIF9ibHVlXzI6ICMzOTQ2ODQsXHJcbiAgX2JsdWVfMzogIzc5N2Q5ZixcclxuICBfYmx1ZV80OiAjMjcyODUxLFxyXG4gIF9ibHVlXzU6ICMzODc2YTcsXHJcbiAgX2JsdWVfNjogIzJjNmRhMSxcclxuICBfYmx1ZV83OiAjMWFhMWM3LFxyXG4gIF9ibHVlXzg6ICMzODNjNjQsXHJcbiAgX3JlZF8xOiAjZjY2MzVlLFxyXG4gIF9yZWRfMjogIzhmMzMzMyxcclxuICBfcmVkXzM6ICNmZjNkNzEsXHJcbiAgX2dyZWVuXzE6ICMyYzdiNDQsXHJcbiAgX2dyZWVuXzI6ICM2NGJiNWQsXHJcbik7XHJcblxyXG4vLyBGT05UU1xyXG5AaW1wb3J0IHVybChcImh0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzP2ZhbWlseT1TaWduaWthXCIpO1xyXG5AaW1wb3J0IHVybChcImh0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzP2ZhbWlseT1GaXJhK1NhbnNcIik7XHJcbkBpbXBvcnQgdXJsKFwiaHR0cHM6Ly9mb250cy5nb29nbGVhcGlzLmNvbS9jc3M/ZmFtaWx5PVppbGxhK1NsYWJcIik7XHJcbkBpbXBvcnQgdXJsKCdodHRwczovL2ZvbnRzLmdvb2dsZWFwaXMuY29tL2NzczI/ZmFtaWx5PUZqYWxsYStPbmUmZGlzcGxheT1zd2FwJyk7XHJcblxyXG4kZm9udHM6IChcclxuICBfc2lnbmlrYV9yZWd1bGFyOiBcIlNpZ25pa2FcIixcclxuICBfZmlyYV9zYW5zOiBcIkZpcmEgU2Fuc1wiLFxyXG4gIF96aWxsYV9zbGFiOiBcIlppbGxhIFNsYWJcIixcclxuICBfZmphbGxhOiBcIkZqYWxsYSBPbmVcIixcclxuKTsiLCJAaW1wb3J0IFwiLi4vLi4vc2hhcmVkL3N0eWxlcy9kZWNsYXJhdGlvbnMuc2Nzc1wiO1xyXG5cclxuZGl2I3Jlc3VtZSBkaXYucm93IHtcclxuICAgIHBhZGRpbmctbGVmdDogNCU7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiA0JTtcclxufVxyXG5cclxuZGl2I3Jlc3VtZSBkaXYudGV4dCBoMiB7XHJcbiAgICBjb2xvcjogbWFwLWdldCgkY29sb3JzLCBfd2hpdGVfMSk7XHJcbiAgICBmb250LXNpemU6IDUwcHg7XHJcbiAgICBmb250LXdlaWdodDogODAwO1xyXG4gICAgdGV4dC1zaGFkb3c6IDVweCAyNXB4IDJweCByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuMSk7XHJcbn1cclxuXHJcbmRpdiNyZXN1bWUgZGl2LmNvbnRhaW5lci1jYWl4YSB7XHJcbiAgICBjb2xvcjogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjcpO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMzBweDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC4xKTtcclxuICAgIHBhZGRpbmc6IDMwcHg7XHJcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbiAgICBoZWlnaHQ6IDI4MHB4O1xyXG4gICAgb3ZlcmZsb3c6IGF1dG87XHJcbn1cclxuXHJcbmRpdiNyZXN1bWUgZGl2LmNvbnRhaW5lci1jYWl4YTo6LXdlYmtpdC1zY3JvbGxiYXIge1xyXG4gICAgd2lkdGg6IDhweDtcclxuICB9XHJcbiAgIFxyXG4gIGRpdiNyZXN1bWUgZGl2LmNvbnRhaW5lci1jYWl4YTo6LXdlYmtpdC1zY3JvbGxiYXItdHJhY2sge1xyXG4gICAgYm94LXNoYWRvdzogaW5zZXQgMCAwIDZweCByZ2JhKDAsIDAsIDAsIDAuMyk7XHJcbiAgfVxyXG4gICBcclxuICBkaXYjcmVzdW1lIGRpdi5jb250YWluZXItY2FpeGE6Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IGRhcmtncmV5O1xyXG4gICAgb3V0bGluZTogMXB4IHNvbGlkIHNsYXRlZ3JleTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDRweDtcclxuICB9XHJcblxyXG5kaXYjcmVzdW1lIGRpdi5jb250YWluZXItY2FpeGEgaDMuc2NoZWR1bGVkIHtcclxuICAgIGNvbG9yOiBtYXAtZ2V0KCRjb2xvcnMsIF95ZWxsb3dfMSk7XHJcbiAgICBmb250LXdlaWdodDogOTAwO1xyXG59XHJcblxyXG5kaXYjcmVzdW1lIGRpdi5jb250YWluZXItY2FpeGEgaDQuaGVhZGluZyB7XHJcbiAgICBjb2xvcjogbWFwLWdldCgkY29sb3JzLCBfd2hpdGVfMSk7XHJcbn1cclxuXHJcbmRpdiNyZXN1bWUgZGl2LmNvbnRhaW5lci1jYWl4YSBoNi5jb21wYW55IHtcclxuICAgIGNvbG9yOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNyk7XHJcbiAgICB0cmFuc2l0aW9uOiBhbGwgMTBzIGVhc2UgMHM7XHJcbn1cclxuXHJcbmRpdiNyZXN1bWUgZGl2LmNvbnRhaW5lci1jYWl4YSBoNi5jb21wYW55IGEge1xyXG4gICAgY29sb3I6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC43KTtcclxuICAgIGJvcmRlci1ib3R0b206IGRvdHRlZCAxcHg7XHJcbiAgICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbn1cclxuXHJcbmRpdiNyZXN1bWUgZGl2LmNvbnRhaW5lci1jYWl4YSBoNi5jb21wYW55IGE6aG92ZXIge1xyXG4gICAgY29sb3I6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC43KTtcclxuICAgIGJvcmRlci1ib3R0b206IHNvbGlkIDFweDtcclxuICAgIFxyXG59XHJcblxyXG5kaXYjcmVzdW1lIGRpdi5jb250YWluZXItY2FpeGEgc3BhbiB7XHJcbiAgICBjb2xvcjogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjcpO1xyXG59XHJcblxyXG5kaXYjcmVzdW1lIGRpdi5jb250YWluZXItZG93bmxvYWQge1xyXG4gICAgcGFkZGluZzogMSUgMDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuZGl2I3Jlc3VtZSBkaXYuY29udGFpbmVyLWRvd25sb2FkIGEge1xyXG4gICAgd2lkdGg6IDE1MHB4O1xyXG4gICAgdGV4dC1kZWNvcmF0aW9uOiBub25lICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbmRpdiNyZXN1bWUgZGl2LmNvbnRhaW5lci1kb3dubG9hZCBkaXYuZG93bmxvYWQtYnV0dG9uIHtcclxuICAgIGhlaWdodDogNDBweDtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBtYXAtZ2V0KCRjb2xvcnMsIF95ZWxsb3dfMSk7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjZmZiZDM5O1xyXG4gICAgY29sb3I6ICMwMDAwMDA7XHJcbiAgICBib3JkZXItcmFkaXVzOiA0MHB4O1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICAgIGxldHRlci1zcGFjaW5nOiAycHg7XHJcbiAgICBmb250LXdlaWdodDogNjAwO1xyXG4gICAgYm94LXNoYWRvdzogMHB4IDI0cHggMzZweCAtMTFweCByZ2JhKDAsIDAsIDAsIDAuMDkpO1xyXG59XHJcblxyXG5kaXYjcmVzdW1lIGRpdi5jb250YWluZXItZG93bmxvYWQgZGl2LmRvd25sb2FkLWJ1dHRvbjpob3ZlciB7XHJcbiAgICBjb2xvcjogd2hpdGVzbW9rZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IG1hcC1nZXQoJGNvbG9ycywgX3llbGxvd18yKTtcclxufSIsIkBpbXBvcnQgdXJsKFwiaHR0cHM6Ly9mb250cy5nb29nbGVhcGlzLmNvbS9jc3M/ZmFtaWx5PVNpZ25pa2FcIik7XG5AaW1wb3J0IHVybChcImh0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzP2ZhbWlseT1GaXJhK1NhbnNcIik7XG5AaW1wb3J0IHVybChcImh0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzP2ZhbWlseT1aaWxsYStTbGFiXCIpO1xuQGltcG9ydCB1cmwoXCJodHRwczovL2ZvbnRzLmdvb2dsZWFwaXMuY29tL2NzczI/ZmFtaWx5PUZqYWxsYStPbmUmZGlzcGxheT1zd2FwXCIpO1xuZGl2I3Jlc3VtZSBkaXYucm93IHtcbiAgcGFkZGluZy1sZWZ0OiA0JTtcbiAgcGFkZGluZy1yaWdodDogNCU7XG59XG5cbmRpdiNyZXN1bWUgZGl2LnRleHQgaDIge1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgZm9udC1zaXplOiA1MHB4O1xuICBmb250LXdlaWdodDogODAwO1xuICB0ZXh0LXNoYWRvdzogNXB4IDI1cHggMnB4IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC4xKTtcbn1cblxuZGl2I3Jlc3VtZSBkaXYuY29udGFpbmVyLWNhaXhhIHtcbiAgY29sb3I6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC43KTtcbiAgbWFyZ2luLWJvdHRvbTogMzBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjEpO1xuICBwYWRkaW5nOiAzMHB4O1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIGhlaWdodDogMjgwcHg7XG4gIG92ZXJmbG93OiBhdXRvO1xufVxuXG5kaXYjcmVzdW1lIGRpdi5jb250YWluZXItY2FpeGE6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcbiAgd2lkdGg6IDhweDtcbn1cblxuZGl2I3Jlc3VtZSBkaXYuY29udGFpbmVyLWNhaXhhOjotd2Via2l0LXNjcm9sbGJhci10cmFjayB7XG4gIGJveC1zaGFkb3c6IGluc2V0IDAgMCA2cHggcmdiYSgwLCAwLCAwLCAwLjMpO1xufVxuXG5kaXYjcmVzdW1lIGRpdi5jb250YWluZXItY2FpeGE6Oi13ZWJraXQtc2Nyb2xsYmFyLXRodW1iIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogZGFya2dyZXk7XG4gIG91dGxpbmU6IDFweCBzb2xpZCBzbGF0ZWdyZXk7XG4gIGJvcmRlci1yYWRpdXM6IDRweDtcbn1cblxuZGl2I3Jlc3VtZSBkaXYuY29udGFpbmVyLWNhaXhhIGgzLnNjaGVkdWxlZCB7XG4gIGNvbG9yOiAjZmJiMDNiO1xuICBmb250LXdlaWdodDogOTAwO1xufVxuXG5kaXYjcmVzdW1lIGRpdi5jb250YWluZXItY2FpeGEgaDQuaGVhZGluZyB7XG4gIGNvbG9yOiAjZmZmZmZmO1xufVxuXG5kaXYjcmVzdW1lIGRpdi5jb250YWluZXItY2FpeGEgaDYuY29tcGFueSB7XG4gIGNvbG9yOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNyk7XG4gIHRyYW5zaXRpb246IGFsbCAxMHMgZWFzZSAwcztcbn1cblxuZGl2I3Jlc3VtZSBkaXYuY29udGFpbmVyLWNhaXhhIGg2LmNvbXBhbnkgYSB7XG4gIGNvbG9yOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNyk7XG4gIGJvcmRlci1ib3R0b206IGRvdHRlZCAxcHg7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbn1cblxuZGl2I3Jlc3VtZSBkaXYuY29udGFpbmVyLWNhaXhhIGg2LmNvbXBhbnkgYTpob3ZlciB7XG4gIGNvbG9yOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNyk7XG4gIGJvcmRlci1ib3R0b206IHNvbGlkIDFweDtcbn1cblxuZGl2I3Jlc3VtZSBkaXYuY29udGFpbmVyLWNhaXhhIHNwYW4ge1xuICBjb2xvcjogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjcpO1xufVxuXG5kaXYjcmVzdW1lIGRpdi5jb250YWluZXItZG93bmxvYWQge1xuICBwYWRkaW5nOiAxJSAwO1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbmRpdiNyZXN1bWUgZGl2LmNvbnRhaW5lci1kb3dubG9hZCBhIHtcbiAgd2lkdGg6IDE1MHB4O1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmUgIWltcG9ydGFudDtcbn1cblxuZGl2I3Jlc3VtZSBkaXYuY29udGFpbmVyLWRvd25sb2FkIGRpdi5kb3dubG9hZC1idXR0b24ge1xuICBoZWlnaHQ6IDQwcHg7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmJiMDNiO1xuICBib3JkZXI6IDFweCBzb2xpZCAjZmZiZDM5O1xuICBjb2xvcjogIzAwMDAwMDtcbiAgYm9yZGVyLXJhZGl1czogNDBweDtcbiAgZm9udC1zaXplOiAxMnB4O1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBsZXR0ZXItc3BhY2luZzogMnB4O1xuICBmb250LXdlaWdodDogNjAwO1xuICBib3gtc2hhZG93OiAwcHggMjRweCAzNnB4IC0xMXB4IHJnYmEoMCwgMCwgMCwgMC4wOSk7XG59XG5cbmRpdiNyZXN1bWUgZGl2LmNvbnRhaW5lci1kb3dubG9hZCBkaXYuZG93bmxvYWQtYnV0dG9uOmhvdmVyIHtcbiAgY29sb3I6IHdoaXRlc21va2U7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmJkMzk7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/pages/resume/resume.component.ts":
  /*!**************************************************!*\
    !*** ./src/app/pages/resume/resume.component.ts ***!
    \**************************************************/

  /*! exports provided: ResumeComponent */

  /***/
  function srcAppPagesResumeResumeComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ResumeComponent", function () {
      return ResumeComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var ResumeComponent = /*#__PURE__*/function () {
      function ResumeComponent() {
        _classCallCheck(this, ResumeComponent);
      }

      _createClass(ResumeComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {}
      }]);

      return ResumeComponent;
    }();

    ResumeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-resume',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./resume.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/resume/resume.component.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./resume.component.scss */
      "./src/app/pages/resume/resume.component.scss"))["default"]]
    })], ResumeComponent);
    /***/
  },

  /***/
  "./src/app/pages/resume/resume.module.ts":
  /*!***********************************************!*\
    !*** ./src/app/pages/resume/resume.module.ts ***!
    \***********************************************/

  /*! exports provided: ResumeModule */

  /***/
  function srcAppPagesResumeResumeModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ResumeModule", function () {
      return ResumeModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _resume_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./resume.component */
    "./src/app/pages/resume/resume.component.ts");
    /* harmony import */


    var _resume_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./resume.routing.module */
    "./src/app/pages/resume/resume.routing.module.ts");
    /* harmony import */


    var src_app_shared_shared_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/app/shared/shared.module */
    "./src/app/shared/shared.module.ts");

    var ResumeModule = function ResumeModule() {
      _classCallCheck(this, ResumeModule);
    };

    ResumeModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      declarations: [_resume_component__WEBPACK_IMPORTED_MODULE_3__["ResumeComponent"]],
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], src_app_shared_shared_module__WEBPACK_IMPORTED_MODULE_5__["SharedModule"], _resume_routing_module__WEBPACK_IMPORTED_MODULE_4__["ResumeRoutingModule"]]
    })], ResumeModule);
    /***/
  },

  /***/
  "./src/app/pages/resume/resume.routing.module.ts":
  /*!*******************************************************!*\
    !*** ./src/app/pages/resume/resume.routing.module.ts ***!
    \*******************************************************/

  /*! exports provided: ResumeRoutingModule */

  /***/
  function srcAppPagesResumeResumeRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ResumeRoutingModule", function () {
      return ResumeRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _resume_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./resume.component */
    "./src/app/pages/resume/resume.component.ts");

    var PagesRoutes = [{
      path: "",
      component: _resume_component__WEBPACK_IMPORTED_MODULE_3__["ResumeComponent"],
      data: {
        params: {
          uriId: 'resume'
        }
      }
    }];

    var ResumeRoutingModule = function ResumeRoutingModule() {
      _classCallCheck(this, ResumeRoutingModule);
    };

    ResumeRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(PagesRoutes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], ResumeRoutingModule);
    /***/
  }
}]);
//# sourceMappingURL=src-app-pages-resume-resume-module-es5.js.map