(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["src-app-pages-about-about-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/about/about.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/about/about.component.html ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div id=\"about\">\r\n    <div class=\"left\">\r\n        <div class=\"container-img animate__animated animate__pulse\">\r\n            <div class=\"mask\"></div>\r\n            <img src=\"assets/emilperezpiz_2.png\" class=\"img-responsive\">\r\n        </div>\r\n    </div>\r\n    <div class=\"right\">\r\n        <div class=\"text animate__animated animate__fadeIn\">\r\n            <h2 class=\"mb-4 mt-1\">{{ 'title.aboutMe' | translate }}</h2>\r\n            <span class=\"heading\">{{ 'message.aboutMe' | translate }}</span>\r\n            <address>\r\n                <h5>{{ 'title.fullName' | translate }}: <span>Emil Perez Piz</span></h5>\r\n                <h5>{{ 'title.birthday' | translate }}: <span>13 de Abril de 1990</span></h5>\r\n                <h5>{{ 'title.email' | translate }}: <span>emilperezpiz@gmail.com</span></h5>\r\n            </address>\r\n            <div class=\"container-download\">\r\n                <a href=\"assets/CV_EMILPEREZPIZ.pdf\" download>\r\n                    <div class=\"download-button\">\r\n                        <span>{{ 'button.downloadCV' | translate }}</span>\r\n                    </div>\r\n                </a>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>");

/***/ }),

/***/ "./src/app/pages/about/about.component.scss":
/*!**************************************************!*\
  !*** ./src/app/pages/about/about.component.scss ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("@import url(\"https://fonts.googleapis.com/css?family=Signika\");\n@import url(\"https://fonts.googleapis.com/css?family=Fira+Sans\");\n@import url(\"https://fonts.googleapis.com/css?family=Zilla+Slab\");\n@import url(\"https://fonts.googleapis.com/css2?family=Fjalla+One&display=swap\");\ndiv#about {\n  display: flex;\n  flex-direction: row;\n}\n@media screen and (max-width: 800px) {\n  div#about {\n    position: relative;\n  }\n}\ndiv#about div.left {\n  flex: 1.5;\n  display: flex;\n  flex-direction: column;\n  justify-content: flex-start;\n  padding: 7% 0 5% 4%;\n}\n@media screen and (max-width: 800px) {\n  div#about div.left {\n    display: none;\n  }\n}\ndiv#about div.left div.container-img {\n  width: 80%;\n  border-radius: 10px;\n  height: 500px;\n  overflow: hidden;\n  position: relative;\n}\ndiv#about div.left div.container-img div.mask {\n  position: absolute;\n  background-color: rgba(0, 0, 0, 0.4);\n  width: 100%;\n  height: 100%;\n}\ndiv#about div.left div.container-img img {\n  max-width: 100%;\n}\ndiv#about div.right {\n  flex: 2;\n  display: flex;\n  flex-direction: column;\n  justify-content: flex-start;\n  padding: 7% 0 5% 4%;\n}\ndiv#about div.right div.text {\n  width: 80%;\n  display: flex;\n  flex-direction: column;\n}\ndiv#about div.right div.text h2 {\n  color: #ffffff;\n  font-size: 50px;\n  font-weight: 800;\n  text-shadow: 5px 25px 2px rgba(255, 255, 255, 0.1);\n}\ndiv#about div.right div.text span.heading {\n  color: rgba(255, 255, 255, 0.7);\n  font-size: 18px;\n}\ndiv#about div.right div.text address {\n  margin-top: 4%;\n  color: #fff;\n}\ndiv#about div.right div.text address h5 span {\n  color: rgba(255, 255, 255, 0.7);\n}\ndiv#about div.right div.text div.container-download {\n  padding: 1% 0;\n  display: flex;\n  align-items: center;\n}\ndiv#about div.right div.text div.container-download a {\n  width: 150px;\n  text-decoration: none !important;\n}\ndiv#about div.right div.text div.container-download div.download-button {\n  height: 40px;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  background-color: #fbb03b;\n  border: 1px solid #ffbd39;\n  color: #000000;\n  border-radius: 40px;\n  font-size: 12px;\n  text-transform: uppercase;\n  letter-spacing: 2px;\n  font-weight: 600;\n  box-shadow: 0px 24px 36px -11px rgba(0, 0, 0, 0.09);\n}\ndiv#about div.right div.text div.container-download div.download-button:hover {\n  color: whitesmoke;\n  background-color: #ffbd39;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYWJvdXQvQzpcXHhhbXBwXFxodGRvY3NcXGVtaWxwZXJlenBpei9zcmNcXGFwcFxcc2hhcmVkXFxzdHlsZXNcXGRlY2xhcmF0aW9ucy5zY3NzIiwic3JjL2FwcC9wYWdlcy9hYm91dC9DOlxceGFtcHBcXGh0ZG9jc1xcZW1pbHBlcmV6cGl6L3NyY1xcYXBwXFxwYWdlc1xcYWJvdXRcXGFib3V0LmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9wYWdlcy9hYm91dC9hYm91dC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFtQ1EsOERBQUE7QUFDQSxnRUFBQTtBQUNBLGlFQUFBO0FBQ0EsK0VBQUE7QUNwQ1I7RUFDSSxhQUFBO0VBQ0EsbUJBQUE7QUNHSjtBRERJO0VBSko7SUFLUSxrQkFBQTtFQ0lOO0FBQ0Y7QUREQTtFQUNJLFNBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSwyQkFBQTtFQUNBLG1CQUFBO0FDSUo7QURGSTtFQVBKO0lBUVEsYUFBQTtFQ0tOO0FBQ0Y7QURGQTtFQUNJLFVBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FDS0o7QURBQTtFQUNJLGtCQUFBO0VBQ0Esb0NBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQ0VKO0FEQ0E7RUFDSSxlQUFBO0FDRUo7QURDQTtFQUNJLE9BQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSwyQkFBQTtFQUNBLG1CQUFBO0FDRUo7QURLQTtFQUNJLFVBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7QUNISjtBRE1BO0VBQ0ksY0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGtEQUFBO0FDSEo7QURNQTtFQUNJLCtCQUFBO0VBQ0EsZUFBQTtBQ0hKO0FETUE7RUFDSSxjQUFBO0VBQ0EsV0FBQTtBQ0hKO0FETUE7RUFDSSwrQkFBQTtBQ0hKO0FETUE7RUFDSSxhQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0FDSEo7QURNQTtFQUNJLFlBQUE7RUFDQSxnQ0FBQTtBQ0hKO0FETUE7RUFDSSxZQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSx5QkFBQTtFQUNBLHlCQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLG1EQUFBO0FDSEo7QURNQTtFQUNJLGlCQUFBO0VBQ0EseUJBQUE7QUNISiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2Fib3V0L2Fib3V0LmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gQ09MT1JTXHJcbiRjb2xvcnM6IChcclxuICBfeWVsbG93XzE6ICNmYmIwM2IsXHJcbiAgX3llbGxvd18yOiAjZmZiZDM5LFxyXG4gIF9ncmF5XzE6ICNiMGI0YzcsXHJcbiAgX2dyYXlfMjogI2M4YzhjOCxcclxuICBfZ3JheV8zOiAjNmY3NzgzLFxyXG4gIF9ncmF5XzQ6ICM0YTRiNWEsXHJcbiAgX2dyYXlfNTogIzc5N2Q5ZixcclxuICBfZ3JheV82OiAjNzA3MDcwLFxyXG4gIF9ncmF5Xzc6ICNjY2MsXHJcbiAgX2dyYXlfODogI2Y3ZjlmYyxcclxuICBfZ3JheV85OiAjZWRmMWY3LFxyXG4gIF93aGl0ZV8xOiAjZmZmZmZmLFxyXG4gIF93aGl0ZV8yOiAjZGRmOGZiLFxyXG4gIF93aGl0ZV8zOiAjZmZjYzk4LFxyXG4gIF93aGl0ZV80OiAjZDdlNGM1LFxyXG4gIF93aGl0ZV81OiAjZjdmOWZjLFxyXG4gIF93aGl0ZV82OiAjZWRmMWY3LFxyXG4gIF9ibHVlXzE6ICMxNGIyZDMsXHJcbiAgX2JsdWVfMjogIzM5NDY4NCxcclxuICBfYmx1ZV8zOiAjNzk3ZDlmLFxyXG4gIF9ibHVlXzQ6ICMyNzI4NTEsXHJcbiAgX2JsdWVfNTogIzM4NzZhNyxcclxuICBfYmx1ZV82OiAjMmM2ZGExLFxyXG4gIF9ibHVlXzc6ICMxYWExYzcsXHJcbiAgX2JsdWVfODogIzM4M2M2NCxcclxuICBfcmVkXzE6ICNmNjYzNWUsXHJcbiAgX3JlZF8yOiAjOGYzMzMzLFxyXG4gIF9yZWRfMzogI2ZmM2Q3MSxcclxuICBfZ3JlZW5fMTogIzJjN2I0NCxcclxuICBfZ3JlZW5fMjogIzY0YmI1ZCxcclxuKTtcclxuXHJcbi8vIEZPTlRTXHJcbkBpbXBvcnQgdXJsKFwiaHR0cHM6Ly9mb250cy5nb29nbGVhcGlzLmNvbS9jc3M/ZmFtaWx5PVNpZ25pa2FcIik7XHJcbkBpbXBvcnQgdXJsKFwiaHR0cHM6Ly9mb250cy5nb29nbGVhcGlzLmNvbS9jc3M/ZmFtaWx5PUZpcmErU2Fuc1wiKTtcclxuQGltcG9ydCB1cmwoXCJodHRwczovL2ZvbnRzLmdvb2dsZWFwaXMuY29tL2Nzcz9mYW1pbHk9WmlsbGErU2xhYlwiKTtcclxuQGltcG9ydCB1cmwoJ2h0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzMj9mYW1pbHk9RmphbGxhK09uZSZkaXNwbGF5PXN3YXAnKTtcclxuXHJcbiRmb250czogKFxyXG4gIF9zaWduaWthX3JlZ3VsYXI6IFwiU2lnbmlrYVwiLFxyXG4gIF9maXJhX3NhbnM6IFwiRmlyYSBTYW5zXCIsXHJcbiAgX3ppbGxhX3NsYWI6IFwiWmlsbGEgU2xhYlwiLFxyXG4gIF9mamFsbGE6IFwiRmphbGxhIE9uZVwiLFxyXG4pOyIsIkBpbXBvcnQgXCIuLi8uLi9zaGFyZWQvc3R5bGVzL2RlY2xhcmF0aW9ucy5zY3NzXCI7XHJcblxyXG5kaXYjYWJvdXQge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcblxyXG4gICAgQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogODAwcHgpIHtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICB9XHJcbn1cclxuXHJcbmRpdiNhYm91dCBkaXYubGVmdCB7XHJcbiAgICBmbGV4OiAxLjU7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcclxuICAgIHBhZGRpbmc6IDclIDAgNSUgNCU7XHJcblxyXG4gICAgQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogODAwcHgpIHtcclxuICAgICAgICBkaXNwbGF5OiBub25lO1xyXG4gICAgfVxyXG59XHJcblxyXG5kaXYjYWJvdXQgZGl2LmxlZnQgZGl2LmNvbnRhaW5lci1pbWcge1xyXG4gICAgd2lkdGg6IDgwJTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgICBoZWlnaHQ6IDUwMHB4O1xyXG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuXHJcbiAgICBAbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjBweCkge31cclxufVxyXG5cclxuZGl2I2Fib3V0IGRpdi5sZWZ0IGRpdi5jb250YWluZXItaW1nIGRpdi5tYXNrIHtcclxuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwgMCwgMCwgMC40KTtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcblxyXG5kaXYjYWJvdXQgZGl2LmxlZnQgZGl2LmNvbnRhaW5lci1pbWcgaW1nIHtcclxuICAgIG1heC13aWR0aDogMTAwJTtcclxufVxyXG5cclxuZGl2I2Fib3V0IGRpdi5yaWdodCB7XHJcbiAgICBmbGV4OiAyO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtc3RhcnQ7XHJcbiAgICBwYWRkaW5nOiA3JSAwIDUlIDQlO1xyXG5cclxuICAgIEBtZWRpYSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDc2MXB4KSBhbmQgKG1heC13aWR0aDogODAwcHgpIHt9XHJcblxyXG4gICAgQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNzYwcHgpIHt9XHJcbn1cclxuXHJcbmRpdiNhYm91dCBkaXYucmlnaHQgZGl2LnRleHQge1xyXG4gICAgd2lkdGg6IDgwJTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG59XHJcblxyXG5kaXYjYWJvdXQgZGl2LnJpZ2h0IGRpdi50ZXh0IGgyIHtcclxuICAgIGNvbG9yOiBtYXAtZ2V0KCRjb2xvcnMsIF93aGl0ZV8xKTtcclxuICAgIGZvbnQtc2l6ZTogNTBweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA4MDA7XHJcbiAgICB0ZXh0LXNoYWRvdzogNXB4IDI1cHggMnB4IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC4xKTtcclxufVxyXG5cclxuZGl2I2Fib3V0IGRpdi5yaWdodCBkaXYudGV4dCBzcGFuLmhlYWRpbmcge1xyXG4gICAgY29sb3I6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC43KTtcclxuICAgIGZvbnQtc2l6ZTogMThweDtcclxufVxyXG5cclxuZGl2I2Fib3V0IGRpdi5yaWdodCBkaXYudGV4dCBhZGRyZXNzIHtcclxuICAgIG1hcmdpbi10b3A6IDQlO1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbn1cclxuXHJcbmRpdiNhYm91dCBkaXYucmlnaHQgZGl2LnRleHQgYWRkcmVzcyBoNSBzcGFuIHtcclxuICAgIGNvbG9yOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNyk7XHJcbn1cclxuXHJcbmRpdiNhYm91dCBkaXYucmlnaHQgZGl2LnRleHQgZGl2LmNvbnRhaW5lci1kb3dubG9hZCB7XHJcbiAgICBwYWRkaW5nOiAxJSAwO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbn1cclxuXHJcbmRpdiNhYm91dCBkaXYucmlnaHQgZGl2LnRleHQgZGl2LmNvbnRhaW5lci1kb3dubG9hZCBhIHtcclxuICAgIHdpZHRoOiAxNTBweDtcclxuICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5kaXYjYWJvdXQgZGl2LnJpZ2h0IGRpdi50ZXh0IGRpdi5jb250YWluZXItZG93bmxvYWQgZGl2LmRvd25sb2FkLWJ1dHRvbiB7XHJcbiAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogbWFwLWdldCgkY29sb3JzLCBfeWVsbG93XzEpO1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI2ZmYmQzOTtcclxuICAgIGNvbG9yOiAjMDAwMDAwO1xyXG4gICAgYm9yZGVyLXJhZGl1czogNDBweDtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XHJcbiAgICBsZXR0ZXItc3BhY2luZzogMnB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAgIGJveC1zaGFkb3c6IDBweCAyNHB4IDM2cHggLTExcHggcmdiYSgwLCAwLCAwLCAwLjA5KTtcclxufVxyXG5cclxuZGl2I2Fib3V0IGRpdi5yaWdodCBkaXYudGV4dCBkaXYuY29udGFpbmVyLWRvd25sb2FkIGRpdi5kb3dubG9hZC1idXR0b246aG92ZXIge1xyXG4gICAgY29sb3I6IHdoaXRlc21va2U7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiBtYXAtZ2V0KCRjb2xvcnMsIF95ZWxsb3dfMik7XHJcbn0iLCJAaW1wb3J0IHVybChcImh0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzP2ZhbWlseT1TaWduaWthXCIpO1xuQGltcG9ydCB1cmwoXCJodHRwczovL2ZvbnRzLmdvb2dsZWFwaXMuY29tL2Nzcz9mYW1pbHk9RmlyYStTYW5zXCIpO1xuQGltcG9ydCB1cmwoXCJodHRwczovL2ZvbnRzLmdvb2dsZWFwaXMuY29tL2Nzcz9mYW1pbHk9WmlsbGErU2xhYlwiKTtcbkBpbXBvcnQgdXJsKFwiaHR0cHM6Ly9mb250cy5nb29nbGVhcGlzLmNvbS9jc3MyP2ZhbWlseT1GamFsbGErT25lJmRpc3BsYXk9c3dhcFwiKTtcbmRpdiNhYm91dCB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiByb3c7XG59XG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA4MDBweCkge1xuICBkaXYjYWJvdXQge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgfVxufVxuXG5kaXYjYWJvdXQgZGl2LmxlZnQge1xuICBmbGV4OiAxLjU7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcbiAgcGFkZGluZzogNyUgMCA1JSA0JTtcbn1cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDgwMHB4KSB7XG4gIGRpdiNhYm91dCBkaXYubGVmdCB7XG4gICAgZGlzcGxheTogbm9uZTtcbiAgfVxufVxuXG5kaXYjYWJvdXQgZGl2LmxlZnQgZGl2LmNvbnRhaW5lci1pbWcge1xuICB3aWR0aDogODAlO1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICBoZWlnaHQ6IDUwMHB4O1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5kaXYjYWJvdXQgZGl2LmxlZnQgZGl2LmNvbnRhaW5lci1pbWcgZGl2Lm1hc2sge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwgMCwgMCwgMC40KTtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbn1cblxuZGl2I2Fib3V0IGRpdi5sZWZ0IGRpdi5jb250YWluZXItaW1nIGltZyB7XG4gIG1heC13aWR0aDogMTAwJTtcbn1cblxuZGl2I2Fib3V0IGRpdi5yaWdodCB7XG4gIGZsZXg6IDI7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIGp1c3RpZnktY29udGVudDogZmxleC1zdGFydDtcbiAgcGFkZGluZzogNyUgMCA1JSA0JTtcbn1cbmRpdiNhYm91dCBkaXYucmlnaHQgZGl2LnRleHQge1xuICB3aWR0aDogODAlO1xuICBkaXNwbGF5OiBmbGV4O1xuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xufVxuXG5kaXYjYWJvdXQgZGl2LnJpZ2h0IGRpdi50ZXh0IGgyIHtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIGZvbnQtc2l6ZTogNTBweDtcbiAgZm9udC13ZWlnaHQ6IDgwMDtcbiAgdGV4dC1zaGFkb3c6IDVweCAyNXB4IDJweCByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuMSk7XG59XG5cbmRpdiNhYm91dCBkaXYucmlnaHQgZGl2LnRleHQgc3Bhbi5oZWFkaW5nIHtcbiAgY29sb3I6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC43KTtcbiAgZm9udC1zaXplOiAxOHB4O1xufVxuXG5kaXYjYWJvdXQgZGl2LnJpZ2h0IGRpdi50ZXh0IGFkZHJlc3Mge1xuICBtYXJnaW4tdG9wOiA0JTtcbiAgY29sb3I6ICNmZmY7XG59XG5cbmRpdiNhYm91dCBkaXYucmlnaHQgZGl2LnRleHQgYWRkcmVzcyBoNSBzcGFuIHtcbiAgY29sb3I6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC43KTtcbn1cblxuZGl2I2Fib3V0IGRpdi5yaWdodCBkaXYudGV4dCBkaXYuY29udGFpbmVyLWRvd25sb2FkIHtcbiAgcGFkZGluZzogMSUgMDtcbiAgZGlzcGxheTogZmxleDtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbn1cblxuZGl2I2Fib3V0IGRpdi5yaWdodCBkaXYudGV4dCBkaXYuY29udGFpbmVyLWRvd25sb2FkIGEge1xuICB3aWR0aDogMTUwcHg7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZSAhaW1wb3J0YW50O1xufVxuXG5kaXYjYWJvdXQgZGl2LnJpZ2h0IGRpdi50ZXh0IGRpdi5jb250YWluZXItZG93bmxvYWQgZGl2LmRvd25sb2FkLWJ1dHRvbiB7XG4gIGhlaWdodDogNDBweDtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmYmIwM2I7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNmZmJkMzk7XG4gIGNvbG9yOiAjMDAwMDAwO1xuICBib3JkZXItcmFkaXVzOiA0MHB4O1xuICBmb250LXNpemU6IDEycHg7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGxldHRlci1zcGFjaW5nOiAycHg7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIGJveC1zaGFkb3c6IDBweCAyNHB4IDM2cHggLTExcHggcmdiYSgwLCAwLCAwLCAwLjA5KTtcbn1cblxuZGl2I2Fib3V0IGRpdi5yaWdodCBkaXYudGV4dCBkaXYuY29udGFpbmVyLWRvd25sb2FkIGRpdi5kb3dubG9hZC1idXR0b246aG92ZXIge1xuICBjb2xvcjogd2hpdGVzbW9rZTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmYmQzOTtcbn0iXX0= */");

/***/ }),

/***/ "./src/app/pages/about/about.component.ts":
/*!************************************************!*\
  !*** ./src/app/pages/about/about.component.ts ***!
  \************************************************/
/*! exports provided: AboutComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutComponent", function() { return AboutComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AboutComponent = class AboutComponent {
    constructor() { }
    ngOnInit() {
    }
};
AboutComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-about',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./about.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/about/about.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./about.component.scss */ "./src/app/pages/about/about.component.scss")).default]
    })
], AboutComponent);



/***/ }),

/***/ "./src/app/pages/about/about.module.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/about/about.module.ts ***!
  \*********************************************/
/*! exports provided: AboutModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutModule", function() { return AboutModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _about_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./about.component */ "./src/app/pages/about/about.component.ts");
/* harmony import */ var src_app_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var _about_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./about.routing.module */ "./src/app/pages/about/about.routing.module.ts");






let AboutModule = class AboutModule {
};
AboutModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_about_component__WEBPACK_IMPORTED_MODULE_3__["AboutComponent"]],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            src_app_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"],
            _about_routing_module__WEBPACK_IMPORTED_MODULE_5__["AboutRoutingModule"]
        ]
    })
], AboutModule);



/***/ }),

/***/ "./src/app/pages/about/about.routing.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/about/about.routing.module.ts ***!
  \*****************************************************/
/*! exports provided: AboutRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AboutRoutingModule", function() { return AboutRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _about_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./about.component */ "./src/app/pages/about/about.component.ts");




const PagesRoutes = [
    {
        path: "", component: _about_component__WEBPACK_IMPORTED_MODULE_3__["AboutComponent"], data: {
            params: { uriId: 'about' }
        }
    }
];
let AboutRoutingModule = class AboutRoutingModule {
};
AboutRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(PagesRoutes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AboutRoutingModule);



/***/ })

}]);
//# sourceMappingURL=src-app-pages-about-about-module-es2015.js.map