(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["src-app-pages-pages-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/pages.component.html":
/*!**********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/pages.component.html ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"show-menu-responsive\">\r\n    <div class=\"top\">\r\n        <div class=\"container-close\" (click)=\"closeMenuResponsive()\"> \r\n            <div class=\"line-close\"></div>    \r\n            <div class=\"line-close\"></div>    \r\n        </div>\r\n    </div>\r\n    <ul>\r\n        <li *ngFor=\"let item of links\" [ngClass]=\"{'actived': item.actived}\"\r\n            (click)=\"redirect(item.uri)\">\r\n            <span>{{ item.text }}</span>\r\n            <div class=\"underlined\">\r\n\r\n            </div>\r\n        </li>\r\n    </ul>\r\n</div>\r\n<app-header></app-header>\r\n<section class=\"all-content\">\r\n    <router-outlet></router-outlet>\r\n</section>\r\n<app-footer></app-footer>");

/***/ }),

/***/ "./src/app/pages/pages.component.scss":
/*!********************************************!*\
  !*** ./src/app/pages/pages.component.scss ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("@import url(\"https://fonts.googleapis.com/css?family=Signika\");\n@import url(\"https://fonts.googleapis.com/css?family=Fira+Sans\");\n@import url(\"https://fonts.googleapis.com/css?family=Zilla+Slab\");\n@import url(\"https://fonts.googleapis.com/css2?family=Fjalla+One&display=swap\");\nsection.all-content {\n  min-height: 75vh;\n  position: relative;\n}\ndiv.show-menu-responsive {\n  width: 100%;\n  color: white;\n  position: absolute;\n  z-index: 9;\n  background-color: #000000;\n  display: none;\n}\ndiv.show-menu-responsive div.top {\n  display: flex;\n  justify-content: flex-end;\n  align-items: center;\n  height: 50px;\n  padding-right: 1%;\n}\ndiv.show-menu-responsive div.top div.container-close div.line-close {\n  width: 35px;\n  height: 3px;\n  background-color: rgba(255, 255, 255, 0.6);\n  border-radius: 24px;\n  margin: 5% 0;\n}\ndiv.show-menu-responsive div.top div.container-close div.line-close:first-child {\n  transform: rotate(50deg);\n}\ndiv.show-menu-responsive div.top div.container-close div.line-close {\n  transform: rotate(-40deg);\n}\ndiv.show-menu-responsive ul {\n  display: none;\n}\n@media screen and (max-width: 800px) {\n  div.show-menu-responsive ul {\n    list-style: none;\n    margin: 0;\n    width: 100%;\n    display: flex;\n    flex-direction: column;\n    justify-content: center;\n    justify-content: space-evenly;\n    align-items: center;\n    height: 100vh;\n  }\n}\ndiv.show-menu-responsive ul li {\n  display: inline-block;\n  line-height: 20px;\n  font-size: 17px;\n  color: #fff;\n  margin-right: 5%;\n  margin-bottom: 1%;\n  font-weight: 400;\n  cursor: pointer;\n}\ndiv.show-menu-responsive ul li span {\n  font-size: 22px;\n}\ndiv.show-menu-responsive ul li.actived {\n  color: #ffbd39;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvQzpcXHhhbXBwXFxodGRvY3NcXGVtaWxwZXJlenBpei9zcmNcXGFwcFxcc2hhcmVkXFxzdHlsZXNcXGRlY2xhcmF0aW9ucy5zY3NzIiwic3JjL2FwcC9wYWdlcy9DOlxceGFtcHBcXGh0ZG9jc1xcZW1pbHBlcmV6cGl6L3NyY1xcYXBwXFxwYWdlc1xccGFnZXMuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3BhZ2VzL3BhZ2VzLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQW1DUSw4REFBQTtBQUNBLGdFQUFBO0FBQ0EsaUVBQUE7QUFDQSwrRUFBQTtBQ3BDUjtFQUNJLGdCQUFBO0VBQ0Esa0JBQUE7QUNHSjtBREFBO0VBQ0ksV0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7RUFDQSx5QkFBQTtFQUNBLGFBQUE7QUNHSjtBREFBO0VBQ0ksYUFBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7QUNHSjtBREFBO0VBQ0ksV0FBQTtFQUNBLFdBQUE7RUFDQSwwQ0FBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtBQ0dKO0FEQUE7RUFDSSx3QkFBQTtBQ0dKO0FEQUE7RUFDSSx5QkFBQTtBQ0dKO0FEQUE7RUFDSSxhQUFBO0FDR0o7QURESTtFQUhKO0lBSVEsZ0JBQUE7SUFDQSxTQUFBO0lBQ0EsV0FBQTtJQUNBLGFBQUE7SUFDQSxzQkFBQTtJQUNBLHVCQUFBO0lBQ0EsNkJBQUE7SUFDQSxtQkFBQTtJQUNBLGFBQUE7RUNJTjtBQUNGO0FEQUE7RUFDSSxxQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0FDR0o7QURBQTtFQUNJLGVBQUE7QUNHSjtBREFBO0VBQ0ksY0FBQTtBQ0dKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvcGFnZXMuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBDT0xPUlNcclxuJGNvbG9yczogKFxyXG4gIF95ZWxsb3dfMTogI2ZiYjAzYixcclxuICBfeWVsbG93XzI6ICNmZmJkMzksXHJcbiAgX2dyYXlfMTogI2IwYjRjNyxcclxuICBfZ3JheV8yOiAjYzhjOGM4LFxyXG4gIF9ncmF5XzM6ICM2Zjc3ODMsXHJcbiAgX2dyYXlfNDogIzRhNGI1YSxcclxuICBfZ3JheV81OiAjNzk3ZDlmLFxyXG4gIF9ncmF5XzY6ICM3MDcwNzAsXHJcbiAgX2dyYXlfNzogI2NjYyxcclxuICBfZ3JheV84OiAjZjdmOWZjLFxyXG4gIF9ncmF5Xzk6ICNlZGYxZjcsXHJcbiAgX3doaXRlXzE6ICNmZmZmZmYsXHJcbiAgX3doaXRlXzI6ICNkZGY4ZmIsXHJcbiAgX3doaXRlXzM6ICNmZmNjOTgsXHJcbiAgX3doaXRlXzQ6ICNkN2U0YzUsXHJcbiAgX3doaXRlXzU6ICNmN2Y5ZmMsXHJcbiAgX3doaXRlXzY6ICNlZGYxZjcsXHJcbiAgX2JsdWVfMTogIzE0YjJkMyxcclxuICBfYmx1ZV8yOiAjMzk0Njg0LFxyXG4gIF9ibHVlXzM6ICM3OTdkOWYsXHJcbiAgX2JsdWVfNDogIzI3Mjg1MSxcclxuICBfYmx1ZV81OiAjMzg3NmE3LFxyXG4gIF9ibHVlXzY6ICMyYzZkYTEsXHJcbiAgX2JsdWVfNzogIzFhYTFjNyxcclxuICBfYmx1ZV84OiAjMzgzYzY0LFxyXG4gIF9yZWRfMTogI2Y2NjM1ZSxcclxuICBfcmVkXzI6ICM4ZjMzMzMsXHJcbiAgX3JlZF8zOiAjZmYzZDcxLFxyXG4gIF9ncmVlbl8xOiAjMmM3YjQ0LFxyXG4gIF9ncmVlbl8yOiAjNjRiYjVkLFxyXG4pO1xyXG5cclxuLy8gRk9OVFNcclxuQGltcG9ydCB1cmwoXCJodHRwczovL2ZvbnRzLmdvb2dsZWFwaXMuY29tL2Nzcz9mYW1pbHk9U2lnbmlrYVwiKTtcclxuQGltcG9ydCB1cmwoXCJodHRwczovL2ZvbnRzLmdvb2dsZWFwaXMuY29tL2Nzcz9mYW1pbHk9RmlyYStTYW5zXCIpO1xyXG5AaW1wb3J0IHVybChcImh0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzP2ZhbWlseT1aaWxsYStTbGFiXCIpO1xyXG5AaW1wb3J0IHVybCgnaHR0cHM6Ly9mb250cy5nb29nbGVhcGlzLmNvbS9jc3MyP2ZhbWlseT1GamFsbGErT25lJmRpc3BsYXk9c3dhcCcpO1xyXG5cclxuJGZvbnRzOiAoXHJcbiAgX3NpZ25pa2FfcmVndWxhcjogXCJTaWduaWthXCIsXHJcbiAgX2ZpcmFfc2FuczogXCJGaXJhIFNhbnNcIixcclxuICBfemlsbGFfc2xhYjogXCJaaWxsYSBTbGFiXCIsXHJcbiAgX2ZqYWxsYTogXCJGamFsbGEgT25lXCIsXHJcbik7IiwiQGltcG9ydCBcIi4uL3NoYXJlZC9zdHlsZXMvZGVjbGFyYXRpb25zLnNjc3NcIjtcclxuXHJcbnNlY3Rpb24uYWxsLWNvbnRlbnQge1xyXG4gICAgbWluLWhlaWdodDogNzV2aDtcclxuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxufVxyXG5cclxuZGl2LnNob3ctbWVudS1yZXNwb25zaXZlIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgY29sb3I6IHdoaXRlO1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgei1pbmRleDogOTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMwMDAwMDA7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG59XHJcblxyXG5kaXYuc2hvdy1tZW51LXJlc3BvbnNpdmUgZGl2LnRvcCB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBmbGV4LWVuZDtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBoZWlnaHQ6IDUwcHg7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAxJTtcclxufVxyXG5cclxuZGl2LnNob3ctbWVudS1yZXNwb25zaXZlIGRpdi50b3AgZGl2LmNvbnRhaW5lci1jbG9zZSBkaXYubGluZS1jbG9zZSB7XHJcbiAgICB3aWR0aDogMzVweDtcclxuICAgIGhlaWdodDogM3B4O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjYpO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMjRweDtcclxuICAgIG1hcmdpbjogNSUgMDtcclxufVxyXG5cclxuZGl2LnNob3ctbWVudS1yZXNwb25zaXZlIGRpdi50b3AgZGl2LmNvbnRhaW5lci1jbG9zZSBkaXYubGluZS1jbG9zZTpmaXJzdC1jaGlsZCB7XHJcbiAgICB0cmFuc2Zvcm06IHJvdGF0ZSg1MGRlZyk7XHJcbn1cclxuXHJcbmRpdi5zaG93LW1lbnUtcmVzcG9uc2l2ZSBkaXYudG9wIGRpdi5jb250YWluZXItY2xvc2UgZGl2LmxpbmUtY2xvc2Uge1xyXG4gICAgdHJhbnNmb3JtOiByb3RhdGUoLTQwZGVnKTtcclxufVxyXG5cclxuZGl2LnNob3ctbWVudS1yZXNwb25zaXZlIHVsIHtcclxuICAgIGRpc3BsYXk6IG5vbmU7XHJcblxyXG4gICAgQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogODAwcHgpIHtcclxuICAgICAgICBsaXN0LXN0eWxlOiBub25lO1xyXG4gICAgICAgIG1hcmdpbjogMDtcclxuICAgICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1ldmVubHk7XHJcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICBoZWlnaHQ6IDEwMHZoO1xyXG4gICAgfVxyXG5cclxufVxyXG5cclxuZGl2LnNob3ctbWVudS1yZXNwb25zaXZlIHVsIGxpIHtcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIGxpbmUtaGVpZ2h0OiAyMHB4O1xyXG4gICAgZm9udC1zaXplOiAxN3B4O1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDUlO1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMSU7XHJcbiAgICBmb250LXdlaWdodDogNDAwO1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG59XHJcblxyXG5kaXYuc2hvdy1tZW51LXJlc3BvbnNpdmUgdWwgbGkgc3BhbiB7XHJcbiAgICBmb250LXNpemU6IDIycHg7XHJcbn1cclxuXHJcbmRpdi5zaG93LW1lbnUtcmVzcG9uc2l2ZSB1bCBsaS5hY3RpdmVkIHtcclxuICAgIGNvbG9yOiBtYXAtZ2V0KCRjb2xvcnMsIF95ZWxsb3dfMik7XHJcbn0iLCJAaW1wb3J0IHVybChcImh0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzP2ZhbWlseT1TaWduaWthXCIpO1xuQGltcG9ydCB1cmwoXCJodHRwczovL2ZvbnRzLmdvb2dsZWFwaXMuY29tL2Nzcz9mYW1pbHk9RmlyYStTYW5zXCIpO1xuQGltcG9ydCB1cmwoXCJodHRwczovL2ZvbnRzLmdvb2dsZWFwaXMuY29tL2Nzcz9mYW1pbHk9WmlsbGErU2xhYlwiKTtcbkBpbXBvcnQgdXJsKFwiaHR0cHM6Ly9mb250cy5nb29nbGVhcGlzLmNvbS9jc3MyP2ZhbWlseT1GamFsbGErT25lJmRpc3BsYXk9c3dhcFwiKTtcbnNlY3Rpb24uYWxsLWNvbnRlbnQge1xuICBtaW4taGVpZ2h0OiA3NXZoO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cbmRpdi5zaG93LW1lbnUtcmVzcG9uc2l2ZSB7XG4gIHdpZHRoOiAxMDAlO1xuICBjb2xvcjogd2hpdGU7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgei1pbmRleDogOTtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAwMDAwMDtcbiAgZGlzcGxheTogbm9uZTtcbn1cblxuZGl2LnNob3ctbWVudS1yZXNwb25zaXZlIGRpdi50b3Age1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBoZWlnaHQ6IDUwcHg7XG4gIHBhZGRpbmctcmlnaHQ6IDElO1xufVxuXG5kaXYuc2hvdy1tZW51LXJlc3BvbnNpdmUgZGl2LnRvcCBkaXYuY29udGFpbmVyLWNsb3NlIGRpdi5saW5lLWNsb3NlIHtcbiAgd2lkdGg6IDM1cHg7XG4gIGhlaWdodDogM3B4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNik7XG4gIGJvcmRlci1yYWRpdXM6IDI0cHg7XG4gIG1hcmdpbjogNSUgMDtcbn1cblxuZGl2LnNob3ctbWVudS1yZXNwb25zaXZlIGRpdi50b3AgZGl2LmNvbnRhaW5lci1jbG9zZSBkaXYubGluZS1jbG9zZTpmaXJzdC1jaGlsZCB7XG4gIHRyYW5zZm9ybTogcm90YXRlKDUwZGVnKTtcbn1cblxuZGl2LnNob3ctbWVudS1yZXNwb25zaXZlIGRpdi50b3AgZGl2LmNvbnRhaW5lci1jbG9zZSBkaXYubGluZS1jbG9zZSB7XG4gIHRyYW5zZm9ybTogcm90YXRlKC00MGRlZyk7XG59XG5cbmRpdi5zaG93LW1lbnUtcmVzcG9uc2l2ZSB1bCB7XG4gIGRpc3BsYXk6IG5vbmU7XG59XG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA4MDBweCkge1xuICBkaXYuc2hvdy1tZW51LXJlc3BvbnNpdmUgdWwge1xuICAgIGxpc3Qtc3R5bGU6IG5vbmU7XG4gICAgbWFyZ2luOiAwO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWV2ZW5seTtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGhlaWdodDogMTAwdmg7XG4gIH1cbn1cblxuZGl2LnNob3ctbWVudS1yZXNwb25zaXZlIHVsIGxpIHtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBsaW5lLWhlaWdodDogMjBweDtcbiAgZm9udC1zaXplOiAxN3B4O1xuICBjb2xvcjogI2ZmZjtcbiAgbWFyZ2luLXJpZ2h0OiA1JTtcbiAgbWFyZ2luLWJvdHRvbTogMSU7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cblxuZGl2LnNob3ctbWVudS1yZXNwb25zaXZlIHVsIGxpIHNwYW4ge1xuICBmb250LXNpemU6IDIycHg7XG59XG5cbmRpdi5zaG93LW1lbnUtcmVzcG9uc2l2ZSB1bCBsaS5hY3RpdmVkIHtcbiAgY29sb3I6ICNmZmJkMzk7XG59Il19 */");

/***/ }),

/***/ "./src/app/pages/pages.component.ts":
/*!******************************************!*\
  !*** ./src/app/pages/pages.component.ts ***!
  \******************************************/
/*! exports provided: PagesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PagesComponent", function() { return PagesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _shared_theme_header_header_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../shared/theme/header/header.component */ "./src/app/shared/theme/header/header.component.ts");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_3__);




let PagesComponent = class PagesComponent {
    constructor() {
        this.links = [];
    }
    ngOnInit() {
        setTimeout(() => {
            this.links = this.header.links;
        }, 700);
    }
    redirect(uri) {
        jquery__WEBPACK_IMPORTED_MODULE_3__('body').removeClass('not-scroll');
        jquery__WEBPACK_IMPORTED_MODULE_3__('div.show-menu-responsive').addClass("animate__animated animate__zoomOut");
        setTimeout(() => {
            jquery__WEBPACK_IMPORTED_MODULE_3__('div.show-menu-responsive').hide(100);
            jquery__WEBPACK_IMPORTED_MODULE_3__('div.show-menu-responsive').removeClass("animate__animated animate__zoomOut");
            this.header.redirect(uri);
        }, 500);
    }
    closeMenuResponsive() {
        jquery__WEBPACK_IMPORTED_MODULE_3__('body').removeClass('not-scroll');
        jquery__WEBPACK_IMPORTED_MODULE_3__('div.show-menu-responsive').addClass("animate__animated animate__zoomOut");
        setTimeout(() => {
            jquery__WEBPACK_IMPORTED_MODULE_3__('div.show-menu-responsive').hide(100);
            jquery__WEBPACK_IMPORTED_MODULE_3__('div.show-menu-responsive').removeClass("animate__animated animate__zoomOut");
        }, 500);
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(_shared_theme_header_header_component__WEBPACK_IMPORTED_MODULE_2__["HeaderComponent"], { static: true })
], PagesComponent.prototype, "header", void 0);
PagesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-pages',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./pages.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/pages.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./pages.component.scss */ "./src/app/pages/pages.component.scss")).default]
    })
], PagesComponent);



/***/ }),

/***/ "./src/app/pages/pages.module.ts":
/*!***************************************!*\
  !*** ./src/app/pages/pages.module.ts ***!
  \***************************************/
/*! exports provided: PagesModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PagesModule", function() { return PagesModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _pages_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./pages.component */ "./src/app/pages/pages.component.ts");
/* harmony import */ var _pages_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./pages.routing.module */ "./src/app/pages/pages.routing.module.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../shared/shared.module */ "./src/app/shared/shared.module.ts");






let PagesModule = class PagesModule {
};
PagesModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_pages_component__WEBPACK_IMPORTED_MODULE_3__["PagesComponent"]],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _shared_shared_module__WEBPACK_IMPORTED_MODULE_5__["SharedModule"],
            _pages_routing_module__WEBPACK_IMPORTED_MODULE_4__["PagesRoutingModule"]
        ]
    })
], PagesModule);



/***/ }),

/***/ "./src/app/pages/pages.routing.module.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/pages.routing.module.ts ***!
  \***********************************************/
/*! exports provided: PagesRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PagesRoutingModule", function() { return PagesRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _pages_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./pages.component */ "./src/app/pages/pages.component.ts");




const PagesRoutes = [
    { path: "", component: _pages_component__WEBPACK_IMPORTED_MODULE_3__["PagesComponent"], loadChildren: "src/app/pages/home/home.module#HomeModule" },
    { path: "sobre", component: _pages_component__WEBPACK_IMPORTED_MODULE_3__["PagesComponent"], loadChildren: "src/app/pages/about/about.module#AboutModule" },
    { path: "resumo", component: _pages_component__WEBPACK_IMPORTED_MODULE_3__["PagesComponent"], loadChildren: "src/app/pages/resume/resume.module#ResumeModule" },
    { path: "contato", component: _pages_component__WEBPACK_IMPORTED_MODULE_3__["PagesComponent"], loadChildren: "src/app/pages/contact/contact.module#ContactModule" }
];
let PagesRoutingModule = class PagesRoutingModule {
};
PagesRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(PagesRoutes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], PagesRoutingModule);



/***/ })

}]);
//# sourceMappingURL=src-app-pages-pages-module-es2015.js.map