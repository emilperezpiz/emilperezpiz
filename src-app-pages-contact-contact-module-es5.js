function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["src-app-pages-contact-contact-module"], {
  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/contact/contact.component.html":
  /*!********************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/contact/contact.component.html ***!
    \********************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppPagesContactContactComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div id=\"contact\">\r\n    <div class=\"row\">\r\n        <div class=\"col-lg-11 col-md-11 col-sm-11\">\r\n            <div class=\"text animate__animated animate__fadeIn\">\r\n                <h4 class=\"mb-4\">{{ 'message.contactMe' | translate }}</h4>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"row\">\r\n        <div class=\"col-lg-11 col-md-11 col-sm-11\">\r\n            <div class=\"container-social-links\">\r\n                <span class=\"slow-show\">Mis Cuentas Personales: </span>\r\n                <div *ngFor=\"let item of socialLinks\" class=\"social-link slow-show\" (click)=\"go(item.href)\">\r\n                    <i class=\"{{ item.iClass }}\"></i>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>";
    /***/
  },

  /***/
  "./src/app/pages/contact/contact.component.scss":
  /*!******************************************************!*\
    !*** ./src/app/pages/contact/contact.component.scss ***!
    \******************************************************/

  /*! exports provided: default */

  /***/
  function srcAppPagesContactContactComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "@import url(\"https://fonts.googleapis.com/css?family=Signika\");\n@import url(\"https://fonts.googleapis.com/css?family=Fira+Sans\");\n@import url(\"https://fonts.googleapis.com/css?family=Zilla+Slab\");\n@import url(\"https://fonts.googleapis.com/css2?family=Fjalla+One&display=swap\");\ndiv#contact div.row {\n  padding-left: 4%;\n  padding-right: 4%;\n}\ndiv#contact div.text h4 {\n  color: #ffffff;\n  font-size: 32px;\n  font-weight: 600;\n  margin-top: 7%;\n}\ndiv#contact div.container-social-links {\n  width: 100%;\n  display: inline-flex;\n  flex-direction: row;\n  align-items: center;\n  margin-top: 3%;\n  flex-wrap: wrap;\n}\ndiv#contact div.container-social-links span {\n  color: rgba(255, 255, 255, 0.6);\n  font-size: 23px;\n  font-weight: 800;\n  margin-right: 3%;\n}\ndiv#contact div.container-social-links div.social-link {\n  width: 60px;\n  height: 60px;\n  background-color: rgba(255, 255, 255, 0.1);\n  border-radius: 100%;\n  margin-right: 4%;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  cursor: pointer;\n  overflow: hidden;\n}\ndiv#contact div.container-social-links div.social-link i {\n  color: rgba(255, 255, 255, 0.6);\n  font-size: 25px;\n  transition: all 0.15s ease 0s;\n}\ndiv#contact div.container-social-links div.social-link i:hover {\n  color: rgba(255, 255, 255, 0.6);\n  transform: scale(1.3);\n}\n.slow-show {\n  -webkit-animation-name: slow-show-opacity;\n          animation-name: slow-show-opacity;\n  -webkit-animation-duration: 5s;\n          animation-duration: 5s;\n}\n@-webkit-keyframes slow-show-opacity {\n  from {\n    opacity: 0;\n  }\n  to {\n    opacity: 1;\n  }\n}\n@keyframes slow-show-opacity {\n  from {\n    opacity: 0;\n  }\n  to {\n    opacity: 1;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvY29udGFjdC9DOlxceGFtcHBcXGh0ZG9jc1xcZW1pbHBlcmV6cGl6L3NyY1xcYXBwXFxzaGFyZWRcXHN0eWxlc1xcZGVjbGFyYXRpb25zLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2NvbnRhY3QvQzpcXHhhbXBwXFxodGRvY3NcXGVtaWxwZXJlenBpei9zcmNcXGFwcFxccGFnZXNcXGNvbnRhY3RcXGNvbnRhY3QuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2NvbnRhY3QvY29udGFjdC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFtQ1EsOERBQUE7QUFDQSxnRUFBQTtBQUNBLGlFQUFBO0FBQ0EsK0VBQUE7QUNwQ1I7RUFDSSxnQkFBQTtFQUNBLGlCQUFBO0FDR0o7QURBQTtFQUNJLGNBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0FDR0o7QURBQTtFQUNJLFdBQUE7RUFDQSxvQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtBQ0dKO0FEQUE7RUFDSSwrQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0FDR0o7QURBQTtFQUNJLFdBQUE7RUFDQSxZQUFBO0VBQ0EsMENBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsYUFBQTtFQUNBLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUNHSjtBREFBO0VBQ0ksK0JBQUE7RUFDQSxlQUFBO0VBQ0EsNkJBQUE7QUNHSjtBREFBO0VBQ0ksK0JBQUE7RUFDQSxxQkFBQTtBQ0dKO0FEQUE7RUFDSSx5Q0FBQTtVQUFBLGlDQUFBO0VBQ0EsOEJBQUE7VUFBQSxzQkFBQTtBQ0dKO0FEQ0E7RUFDSTtJQUNJLFVBQUE7RUNFTjtFRENFO0lBQ0ksVUFBQTtFQ0NOO0FBQ0Y7QURSQTtFQUNJO0lBQ0ksVUFBQTtFQ0VOO0VEQ0U7SUFDSSxVQUFBO0VDQ047QUFDRiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2NvbnRhY3QvY29udGFjdC5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8vIENPTE9SU1xyXG4kY29sb3JzOiAoXHJcbiAgX3llbGxvd18xOiAjZmJiMDNiLFxyXG4gIF95ZWxsb3dfMjogI2ZmYmQzOSxcclxuICBfZ3JheV8xOiAjYjBiNGM3LFxyXG4gIF9ncmF5XzI6ICNjOGM4YzgsXHJcbiAgX2dyYXlfMzogIzZmNzc4MyxcclxuICBfZ3JheV80OiAjNGE0YjVhLFxyXG4gIF9ncmF5XzU6ICM3OTdkOWYsXHJcbiAgX2dyYXlfNjogIzcwNzA3MCxcclxuICBfZ3JheV83OiAjY2NjLFxyXG4gIF9ncmF5Xzg6ICNmN2Y5ZmMsXHJcbiAgX2dyYXlfOTogI2VkZjFmNyxcclxuICBfd2hpdGVfMTogI2ZmZmZmZixcclxuICBfd2hpdGVfMjogI2RkZjhmYixcclxuICBfd2hpdGVfMzogI2ZmY2M5OCxcclxuICBfd2hpdGVfNDogI2Q3ZTRjNSxcclxuICBfd2hpdGVfNTogI2Y3ZjlmYyxcclxuICBfd2hpdGVfNjogI2VkZjFmNyxcclxuICBfYmx1ZV8xOiAjMTRiMmQzLFxyXG4gIF9ibHVlXzI6ICMzOTQ2ODQsXHJcbiAgX2JsdWVfMzogIzc5N2Q5ZixcclxuICBfYmx1ZV80OiAjMjcyODUxLFxyXG4gIF9ibHVlXzU6ICMzODc2YTcsXHJcbiAgX2JsdWVfNjogIzJjNmRhMSxcclxuICBfYmx1ZV83OiAjMWFhMWM3LFxyXG4gIF9ibHVlXzg6ICMzODNjNjQsXHJcbiAgX3JlZF8xOiAjZjY2MzVlLFxyXG4gIF9yZWRfMjogIzhmMzMzMyxcclxuICBfcmVkXzM6ICNmZjNkNzEsXHJcbiAgX2dyZWVuXzE6ICMyYzdiNDQsXHJcbiAgX2dyZWVuXzI6ICM2NGJiNWQsXHJcbik7XHJcblxyXG4vLyBGT05UU1xyXG5AaW1wb3J0IHVybChcImh0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzP2ZhbWlseT1TaWduaWthXCIpO1xyXG5AaW1wb3J0IHVybChcImh0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzP2ZhbWlseT1GaXJhK1NhbnNcIik7XHJcbkBpbXBvcnQgdXJsKFwiaHR0cHM6Ly9mb250cy5nb29nbGVhcGlzLmNvbS9jc3M/ZmFtaWx5PVppbGxhK1NsYWJcIik7XHJcbkBpbXBvcnQgdXJsKCdodHRwczovL2ZvbnRzLmdvb2dsZWFwaXMuY29tL2NzczI/ZmFtaWx5PUZqYWxsYStPbmUmZGlzcGxheT1zd2FwJyk7XHJcblxyXG4kZm9udHM6IChcclxuICBfc2lnbmlrYV9yZWd1bGFyOiBcIlNpZ25pa2FcIixcclxuICBfZmlyYV9zYW5zOiBcIkZpcmEgU2Fuc1wiLFxyXG4gIF96aWxsYV9zbGFiOiBcIlppbGxhIFNsYWJcIixcclxuICBfZmphbGxhOiBcIkZqYWxsYSBPbmVcIixcclxuKTsiLCJAaW1wb3J0IFwiLi4vLi4vc2hhcmVkL3N0eWxlcy9kZWNsYXJhdGlvbnMuc2Nzc1wiO1xyXG5cclxuZGl2I2NvbnRhY3QgZGl2LnJvdyB7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDQlO1xyXG4gICAgcGFkZGluZy1yaWdodDogNCU7XHJcbn1cclxuXHJcbmRpdiNjb250YWN0IGRpdi50ZXh0IGg0IHtcclxuICAgIGNvbG9yOiBtYXAtZ2V0KCRjb2xvcnMsIF93aGl0ZV8xKTtcclxuICAgIGZvbnQtc2l6ZTogMzJweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgICBtYXJnaW4tdG9wOiA3JTtcclxufVxyXG5cclxuZGl2I2NvbnRhY3QgZGl2LmNvbnRhaW5lci1zb2NpYWwtbGlua3Mge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiByb3c7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgbWFyZ2luLXRvcDogMyU7XHJcbiAgICBmbGV4LXdyYXA6IHdyYXA7XHJcbn1cclxuXHJcbmRpdiNjb250YWN0IGRpdi5jb250YWluZXItc29jaWFsLWxpbmtzIHNwYW4ge1xyXG4gICAgY29sb3I6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC42KTtcclxuICAgIGZvbnQtc2l6ZTogMjNweDtcclxuICAgIGZvbnQtd2VpZ2h0OiA4MDA7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDMlO1xyXG59XHJcblxyXG5kaXYjY29udGFjdCBkaXYuY29udGFpbmVyLXNvY2lhbC1saW5rcyBkaXYuc29jaWFsLWxpbmsge1xyXG4gICAgd2lkdGg6IDYwcHg7XHJcbiAgICBoZWlnaHQ6IDYwcHg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuMSk7XHJcbiAgICBib3JkZXItcmFkaXVzOiAxMDAlO1xyXG4gICAgbWFyZ2luLXJpZ2h0OiA0JTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xyXG59XHJcblxyXG5kaXYjY29udGFjdCBkaXYuY29udGFpbmVyLXNvY2lhbC1saW5rcyBkaXYuc29jaWFsLWxpbmsgaSB7XHJcbiAgICBjb2xvcjogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjYpO1xyXG4gICAgZm9udC1zaXplOiAyNXB4O1xyXG4gICAgdHJhbnNpdGlvbjogYWxsIDAuMTVzIGVhc2UgMHM7XHJcbn1cclxuXHJcbmRpdiNjb250YWN0IGRpdi5jb250YWluZXItc29jaWFsLWxpbmtzIGRpdi5zb2NpYWwtbGluayBpOmhvdmVyIHtcclxuICAgIGNvbG9yOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNik7XHJcbiAgICB0cmFuc2Zvcm06IHNjYWxlKDEuMyk7XHJcbn1cclxuXHJcbi5zbG93LXNob3cge1xyXG4gICAgYW5pbWF0aW9uLW5hbWU6IHNsb3ctc2hvdy1vcGFjaXR5O1xyXG4gICAgYW5pbWF0aW9uLWR1cmF0aW9uOiA1cztcclxuXHJcbn1cclxuXHJcbkBrZXlmcmFtZXMgc2xvdy1zaG93LW9wYWNpdHkge1xyXG4gICAgZnJvbSB7XHJcbiAgICAgICAgb3BhY2l0eTogMDtcclxuICAgIH1cclxuXHJcbiAgICB0byB7XHJcbiAgICAgICAgb3BhY2l0eTogMTtcclxuICAgIH1cclxufSIsIkBpbXBvcnQgdXJsKFwiaHR0cHM6Ly9mb250cy5nb29nbGVhcGlzLmNvbS9jc3M/ZmFtaWx5PVNpZ25pa2FcIik7XG5AaW1wb3J0IHVybChcImh0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzP2ZhbWlseT1GaXJhK1NhbnNcIik7XG5AaW1wb3J0IHVybChcImh0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzP2ZhbWlseT1aaWxsYStTbGFiXCIpO1xuQGltcG9ydCB1cmwoXCJodHRwczovL2ZvbnRzLmdvb2dsZWFwaXMuY29tL2NzczI/ZmFtaWx5PUZqYWxsYStPbmUmZGlzcGxheT1zd2FwXCIpO1xuZGl2I2NvbnRhY3QgZGl2LnJvdyB7XG4gIHBhZGRpbmctbGVmdDogNCU7XG4gIHBhZGRpbmctcmlnaHQ6IDQlO1xufVxuXG5kaXYjY29udGFjdCBkaXYudGV4dCBoNCB7XG4gIGNvbG9yOiAjZmZmZmZmO1xuICBmb250LXNpemU6IDMycHg7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG4gIG1hcmdpbi10b3A6IDclO1xufVxuXG5kaXYjY29udGFjdCBkaXYuY29udGFpbmVyLXNvY2lhbC1saW5rcyB7XG4gIHdpZHRoOiAxMDAlO1xuICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgbWFyZ2luLXRvcDogMyU7XG4gIGZsZXgtd3JhcDogd3JhcDtcbn1cblxuZGl2I2NvbnRhY3QgZGl2LmNvbnRhaW5lci1zb2NpYWwtbGlua3Mgc3BhbiB7XG4gIGNvbG9yOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNik7XG4gIGZvbnQtc2l6ZTogMjNweDtcbiAgZm9udC13ZWlnaHQ6IDgwMDtcbiAgbWFyZ2luLXJpZ2h0OiAzJTtcbn1cblxuZGl2I2NvbnRhY3QgZGl2LmNvbnRhaW5lci1zb2NpYWwtbGlua3MgZGl2LnNvY2lhbC1saW5rIHtcbiAgd2lkdGg6IDYwcHg7XG4gIGhlaWdodDogNjBweDtcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjEpO1xuICBib3JkZXItcmFkaXVzOiAxMDAlO1xuICBtYXJnaW4tcmlnaHQ6IDQlO1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgY3Vyc29yOiBwb2ludGVyO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuXG5kaXYjY29udGFjdCBkaXYuY29udGFpbmVyLXNvY2lhbC1saW5rcyBkaXYuc29jaWFsLWxpbmsgaSB7XG4gIGNvbG9yOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNik7XG4gIGZvbnQtc2l6ZTogMjVweDtcbiAgdHJhbnNpdGlvbjogYWxsIDAuMTVzIGVhc2UgMHM7XG59XG5cbmRpdiNjb250YWN0IGRpdi5jb250YWluZXItc29jaWFsLWxpbmtzIGRpdi5zb2NpYWwtbGluayBpOmhvdmVyIHtcbiAgY29sb3I6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC42KTtcbiAgdHJhbnNmb3JtOiBzY2FsZSgxLjMpO1xufVxuXG4uc2xvdy1zaG93IHtcbiAgYW5pbWF0aW9uLW5hbWU6IHNsb3ctc2hvdy1vcGFjaXR5O1xuICBhbmltYXRpb24tZHVyYXRpb246IDVzO1xufVxuXG5Aa2V5ZnJhbWVzIHNsb3ctc2hvdy1vcGFjaXR5IHtcbiAgZnJvbSB7XG4gICAgb3BhY2l0eTogMDtcbiAgfVxuICB0byB7XG4gICAgb3BhY2l0eTogMTtcbiAgfVxufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/pages/contact/contact.component.ts":
  /*!****************************************************!*\
    !*** ./src/app/pages/contact/contact.component.ts ***!
    \****************************************************/

  /*! exports provided: ContactComponent */

  /***/
  function srcAppPagesContactContactComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ContactComponent", function () {
      return ContactComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    var ContactComponent = /*#__PURE__*/function () {
      function ContactComponent() {
        _classCallCheck(this, ContactComponent);
      }

      _createClass(ContactComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.socialLinks = [{
            name: "Twitter",
            href: "https://twitter.com/emilperezpiz",
            iClass: "fab fa-twitter",
            show: false
          }, {
            name: "LinkedId",
            href: "https://www.linkedin.com/in/emil-perez-piz/",
            iClass: "fab fa-linkedin-in",
            show: false
          }, {
            name: "Facebook",
            href: "https://www.facebook.com/emilpiz",
            iClass: "fab fa-facebook-f",
            show: false
          }, {
            name: "Mail",
            href: "mailto:emilperezpiz@gmail.com",
            iClass: "fas fa-envelope-square",
            show: false
          }];
        }
      }, {
        key: "go",
        value: function go(url) {
          location.href = url;
        }
      }]);

      return ContactComponent;
    }();

    ContactComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-contact',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./contact.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/pages/contact/contact.component.html"))["default"],
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./contact.component.scss */
      "./src/app/pages/contact/contact.component.scss"))["default"]]
    })], ContactComponent);
    /***/
  },

  /***/
  "./src/app/pages/contact/contact.module.ts":
  /*!*************************************************!*\
    !*** ./src/app/pages/contact/contact.module.ts ***!
    \*************************************************/

  /*! exports provided: ContactModule */

  /***/
  function srcAppPagesContactContactModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ContactModule", function () {
      return ContactModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _contact_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./contact.component */
    "./src/app/pages/contact/contact.component.ts");
    /* harmony import */


    var src_app_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/app/shared/shared.module */
    "./src/app/shared/shared.module.ts");
    /* harmony import */


    var _contact_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./contact.routing.module */
    "./src/app/pages/contact/contact.routing.module.ts");

    var ContactModule = function ContactModule() {
      _classCallCheck(this, ContactModule);
    };

    ContactModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      declarations: [_contact_component__WEBPACK_IMPORTED_MODULE_3__["ContactComponent"]],
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], src_app_shared_shared_module__WEBPACK_IMPORTED_MODULE_4__["SharedModule"], _contact_routing_module__WEBPACK_IMPORTED_MODULE_5__["ContactRoutingModule"]]
    })], ContactModule);
    /***/
  },

  /***/
  "./src/app/pages/contact/contact.routing.module.ts":
  /*!*********************************************************!*\
    !*** ./src/app/pages/contact/contact.routing.module.ts ***!
    \*********************************************************/

  /*! exports provided: ContactRoutingModule */

  /***/
  function srcAppPagesContactContactRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ContactRoutingModule", function () {
      return ContactRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _contact_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./contact.component */
    "./src/app/pages/contact/contact.component.ts");

    var PagesRoutes = [{
      path: "",
      component: _contact_component__WEBPACK_IMPORTED_MODULE_3__["ContactComponent"],
      data: {
        params: {
          uriId: 'contact'
        }
      }
    }];

    var ContactRoutingModule = function ContactRoutingModule() {
      _classCallCheck(this, ContactRoutingModule);
    };

    ContactRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(PagesRoutes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], ContactRoutingModule);
    /***/
  }
}]);
//# sourceMappingURL=src-app-pages-contact-contact-module-es5.js.map